"""Given an input word file, select for output exactly those words
   meeting a given filtering criterion.  For example, words of length k,
   words containing a "q", words that are palindromes, etc."""

def filter( word, f ):
    """This returns true or false depending on
    whether or not the word passes the filter.
    f is a functional parameter."""
    return f( word )

def filterFile( inputFileName, outputFileName, f ):
    """Given a file of words, create a new file containing
    those satisfying a filter function f."""
    # Open the input and output files
    inputFile = open(inputFileName, 'r')
    outputFile = open(outputFileName, 'w')

    # We'll count both words input and words output.
    inputcount = 0
    outputcount = 0
    for line in inputFile:
        inputcount += 1
        # Remove extraneous whitespace, if any.
        word = line.strip()
        # Apply the filtering function f. 
        if filter( word, f ):
            outputFile.write( word + "\n" )
            outputcount += 1
            # print( word )
    # Report the words input and output.
    print ("Transferred ", outputcount, " of ", \
               inputcount, " words.")
    # Close both input and output files. 
    inputFile.close()
    outputFile.close()

def isPalindrome( word ):
    """This returns true if the word is a palindrome."""
    

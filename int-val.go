// golang

// Get unique values from the array of int values

func unique(arr []int) []int {
	var unique []int
	for _, v := range arr {
		if !contains(unique, v) {
			unique = append(unique, v)
		}
	}
	return unique
}

func contains(arr []int, val int) bool {
	for _, v := range arr {
		if v == val {
			return true
		}
	}
	return false
}

func main() {
	arr := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	fmt.Println(unique(arr))
}